import React from 'react';
import {
  View, Text, StyleSheet,
  TouchableNativeFeedback
} from 'react-native';

class Todo extends React.Component {
  render() {
    return (
      <TouchableNativeFeedback
        onPress={()=>{
          this
            .props
            .navigation
            .navigate('TodoDetails',{
              todo: this.props.todo,
            })
        }}
      >
        <View style={styles.container}>
          <Text style={styles.text}>
            {this.props.todo.text}
          </Text>
        </View>
      </TouchableNativeFeedback>
    )
  }
}

const styles = StyleSheet.create({
  container:{
    width: '100%',
    padding: 7.5,
    borderRadius: 5,
    marginBottom: 7.5,
    backgroundColor: 'white',
    // shadow to iOS
    // shadowColor: 'black',
    // shadowOffset: {width: 8, height: 8,},
    // shadowOpacity: 0.3,
    // shadowRadius: 5,
    
    // shadow to android
    elevation: 5,
  },
  text:{

  }
})

export default Todo;
