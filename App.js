import React, {Component} from 'react';
import { StyleSheet, View, ScrollView, Text, PermissionsAndroid } from 'react-native';
import TodoList from './components/todo-list';
import AddTodo from './components/add-todo';
import { createStackNavigator, createAppContainer } from "react-navigation";

//colors: https://material.io/tools/color/#!/?view.left=0&view.right=0&primary.color=5E35B1&secondary.color=7C4DFF

const defaultNavigationOptions = {
  headerStyle:{
    // display: 'none',
    backgroundColor: '#140078',
  },
  headerTintColor: 'white',
  headerTitleStyle: {
    fontWeight: 'bold',
    color: 'white',
  },
}

class TodoDetails extends Component {
  static navigationOptions = {
    ...defaultNavigationOptions,
    title: 'Todo'
  }
  render(){
    const todo = this.props.navigation.getParam('todo');
    return(
      <View>
        <Text>
          {todo.text}
        </Text>
        <Text>
          Created at: {todo.location}
        </Text>
      </View>
    )
  }
}

class Home extends Component {
  static navigationOptions = {
    ...defaultNavigationOptions,
    title: 'Todo App',
    
  };
  constructor(props) {
    super(props);
    // setTimeout(()=>{
    //   this.props.navigation.navigate('TodoDetails',{
    //     text: 'e aí galerinha'
    //   });
    // },3000);
    
    const todo1 = {
      id: 1, text: 'fazer o app bonitao o/',
    };
    const todo2 = {
      id: 2, text: 'fazer o app maaais bonitao',
    };
    const todo3 = {
      id: 3, text: 'lanchar',
    };
    this.state = {
      idCount: 3,
      todos: [todo1, todo2, todo3],
    }
    this.requestMapsPermission();
  }

  async requestMapsPermission(){
    try{
      const isGranted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          'title': 'Todo app location access',
          'message': 'We need your location to know where you are'
        }
      )
      // console.warn(isGranted);
      this.setState({
        // geolocationPermissionGranted: isGranted === 'granted',
        geolocationPermissionGranted: isGranted,
      })
    } catch(err){
      // console.error(err);
      return;
    }
  }

  // setTodoLocation(id,coords){
  //   const {latitude,longitude}=coords; //talvez n precise comentar
  //   const {todos} = this.state;
  //   todos
  //     .find(todo=>todo.id === id)
  //     .location = coords;
  //   this.setState({
  //     todos: todos
  //   })
  // }

  async setTodoLocation(id,coords){
    const {latitude,longitude}=coords;
    
    try{
      const response = await fetch(
        `https://maps.googleapis.com/maps/api/geocode/json?latlng=${latitude},${longitude}&key=${KEY_ADDRESS}`
      );
      const data = await response.json();

      

      if (!data.error_message){
        const address = data.results[0].formatted_address;
  
        const { todos } = this.state;
        todos
          .find(todo => todo.id === id)
          .location = address;
        this.setState({
          todos
        })
      } else{
        throw JSON.stringify(data);
      }
    } catch(e){
      console.error(e);
    }
  }

  addTodo(text) {
    const id= this.state.idCount +1;
    this.setState({
      todos: [{ id: id, text:text }].concat(this.state.todos),
      idCount: id,
    // }, ()=> console.warn(this.state.todos));    
    });
    if (this.state.geolocationPermissionGranted){
      navigator.geolocation.getCurrentPosition((pos) => {
        this.setTodoLocation(id,pos.coords)      
      }, null, {enableHighAccuracy: true})
    }
  }
  
  render() {
    return (
      <View style={styles.container}>        
        <AddTodo add={text => this.addTodo(text)}/>
        <ScrollView contentContainerStyle={styles.scrollView}>          
          <TodoList
            todoList={this.state.todos}
            navigation={this.props.navigation}
          />
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  },
  scrollView:{
    width: '100%',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },  
});

const AppNavigator = createStackNavigator({
  Home: { screen: Home },
  TodoDetails: { screen: TodoDetails }
});

export default createAppContainer(AppNavigator);