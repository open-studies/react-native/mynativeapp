/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
// import {Platform, StyleSheet, Text, View} from 'react-native';

// const instructions = Platform.select({
//   ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
//   android:
//     'Double tap R on your keyboard to reload,\n' +
//     'Shake or press menu button for dev menu',
// });

// class Hello extends Component{
//   // constructor(){
//   //   super();
//   //   this.state = {
//   //     name: 'Igor',
//   //   }
//   //   setTimeout(() => {
//   //     this.setState({
//   //       name: 'Naturo'
//   //     })
//   //   },5000);
//   // }
//   render(){
//     return(
//       <Text
//         style={styles.welcome}
//       >
//         {/* Welcome! */}
//         {/* Welcome {this.state.name}! */}
//         Welcome, {this.props.name}!
//       </Text>
//     );
//   }
// }

export default class App extends Component {
  constructor(){
    super();
    this.state = {
      todos: [],
    }
  }
  render() {
    return (
      <View style={styles.container}>
        {/* <Hello name={this.state.name}/>        
        <Text style={styles.instructions}>To get started, edit App.js</Text>
        <Text style={styles.instructions}>{instructions}</Text> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
